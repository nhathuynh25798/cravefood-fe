import { types } from './global.meta';
import { handleActions } from 'redux-actions';

const defaultLocationState = {
  globalValue: {},
};

const getGlobalValueSuccess = (state, action) => ({
  ...state,
  globalValue: action.payload,
});

export default handleActions(
  {
    [types.FETCH_GLOBAL_VALUE_SUCCESS]: getGlobalValueSuccess,
  },
  defaultLocationState
);
