import { types } from './product.meta';
import { handleActions } from 'redux-actions';

const defaultProductState = {
  products: [],
  bySlug: {},
  searchProducts: [],
};

const getProductsSuccess = (state, action) => ({
  ...state,
  products: action.payload.data,
});

const getProductBySlugSuccess = (state, action) => ({
  ...state,
  bySlug: action.payload,
});

export default handleActions(
  {
    [types.FETCH_PRODUCT_SUCCESS]: getProductsSuccess,
    [types.FETCH_PRODUCT_BY_SLUG_SUCCESS]: getProductBySlugSuccess,
  },
  defaultProductState
);
