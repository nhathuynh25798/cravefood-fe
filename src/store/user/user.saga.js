import { takeLatest, call, put, all } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';
import jwtDecode from 'jwt-decode';

import { types, actionCreator } from './user.meta';
import { actionCreator as pageActionCreator } from '../page/page.meta';

import {
  customerRegister,
  customerLogin,
  addWishList,
  deleteWishList,
  getUser,
  updateUser,
  getWishList,
  getAddresses,
  addAddress,
  deleteAddress,
  getOrders,
} from 'api/apiRouter';
import cookie from 'js-cookie';
import { sleep } from 'utils';
import { sagaErrorWrapper } from 'utils/error';

function* sagaRegister(action) {
  let { email, phone, password } = action.payload;
  const { body } = yield call(customerRegister, {
    email,
    phone,
    password,
  });
  yield call(notification.success, {
    message: body?.message,
  });
  yield call(sleep, 500);

  yield put(actionCreator.login({ email: email, password: password }));
}

function* sagaLogin(action) {
  let { email, password } = action.payload;
  const { body } = yield call(customerLogin, {
    email,
    password,
  });

  const { token } = body;
  const { exp, user } = jwtDecode(token);

  cookie.set('token', token, { expires: exp });

  yield put(
    actionCreator.loginSuccess({
      user: {
        ...user,
        loginIn: true,
      },
    })
  );
  yield call(notification.success, {
    message: 'Đăng nhập thành công!',
  });
  yield put(pageActionCreator.setLoading(false));

  yield navigate('/');
}

function* sagaLogout(action) {
  cookie.remove('token');
  yield put(actionCreator.logoutSuccess());
  yield call(notification.success, {
    message: 'Đăng xuất thành công!',
  });
  yield navigate('/');
}

// == wishlist ==
function* sagaGetProductsWishLists(action) {
  const { userId } = action.payload;
  const { body } = yield call(getWishList, userId);

  yield put(actionCreator.getProductWishListsSuccess(body));
}

function* sagaAddWishLists(action) {
  const { userId, productId } = action.payload;
  const { body } = yield call(addWishList, {
    userId,
    productId,
  });

  yield call(notification.success, {
    message: body?.message,
  });

  const res = yield call(getUser, userId);

  yield put(actionCreator.addWishListsSuccess({ ...res?.body }));
}

function* sagaDeleteWishLists(action) {
  const { userId, productId } = action.payload;
  const { body } = yield call(deleteWishList, {
    userId,
    productId,
  });

  yield call(notification.success, {
    message: body?.message,
  });

  const res = yield call(getUser, userId);

  yield put(actionCreator.addWishListsSuccess({ ...res?.body }));
}

// == update user ==
function* sagaUpdateUser(action) {
  const { id, name, phone, gender, birthday } = action.payload;
  const { body } = yield call(updateUser, {
    id,
    name,
    phone,
    gender,
    birthday,
  });

  yield call(notification.success, {
    message: body?.message,
  });

  const res = yield call(getUser, id);
  yield put(actionCreator.updateUserSuccess({ ...res?.body }));
}

// == addresses ==
function* sagaGetAddress(action) {
  const { userId } = action.payload;
  const { body } = yield call(getAddresses, userId);

  yield put(actionCreator.getAddressSuccess(body));
}

function* sagaAddAddress(action) {
  const { userId, data } = action.payload;
  const { body } = yield call(addAddress, { addressList: data });

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreator.getAddress({ userId: userId }));
}

function* sagaDeleteAddress(action) {
  const { userId, id } = action.payload;
  const { body } = yield call(deleteAddress, id);

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreator.getAddress({ userId: userId }));
}

function* sagaGetOrder(action) {
  const { userId } = action.payload;
  const { body } = yield call(getOrders, userId);

  yield put(actionCreator.actGetOrderSuccess(body));
}

export default function* userSaga() {
  yield all([
    takeLatest(types.LOGIN, sagaErrorWrapper(sagaLogin)),
    takeLatest(types.REGISTER, sagaErrorWrapper(sagaRegister)),
    takeLatest(types.LOGOUT, sagaErrorWrapper(sagaLogout)),
    takeLatest(types.GET_WISHLIST, sagaErrorWrapper(sagaGetProductsWishLists)),
    takeLatest(types.ADD_WISHLISTS, sagaErrorWrapper(sagaAddWishLists)),
    takeLatest(types.REMOVE_WISHLISTS, sagaErrorWrapper(sagaDeleteWishLists)),
    takeLatest(types.UPDATE_USER, sagaErrorWrapper(sagaUpdateUser)),
    takeLatest(types.GET_ADDRESSES, sagaErrorWrapper(sagaGetAddress)),
    takeLatest(types.ADD_ADDRESS, sagaErrorWrapper(sagaAddAddress)),
    takeLatest(types.REMOVE_ADDRESS, sagaErrorWrapper(sagaDeleteAddress)),
    takeLatest(types.GET_ORDER, sagaErrorWrapper(sagaGetOrder)),
  ]);
}
