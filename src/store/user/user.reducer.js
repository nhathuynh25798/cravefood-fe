import { handleActions } from 'redux-actions';
import { types } from './user.meta';

const initialState = {
  user: null,
  addresses: [],
  wishLists: [],
  orders: [],
};

const loginSuccess = (state, { payload }) => ({
  ...state,
  user: payload.user,
});

const logoutSuccess = (state) => ({
  user: null,
  addresses: [],
  wishLists: [],
  orders: [],
});

const getProductsWishListSuccess = (state, action) => ({
  ...state,
  wishLists: action.payload,
});

const getOrdersSuccess = (state, action) => ({
  ...state,
  orders: action.payload,
});

const addWishListsSuccess = (state, action) => ({
  ...state,
  user: action.payload,
});

const removeWishListsSuccess = (state, action) => ({
  ...state,
  user: action.payload,
});

const updateUserSuccess = (state, action) => ({
  ...state,
  user: action.payload,
});

const getAddressesSuccess = (state, action) => ({
  ...state,
  addresses: action.payload,
});

export default handleActions(
  {
    [types.LOGIN_SUCCESS]: loginSuccess,
    [types.LOGOUT_SUCCESS]: logoutSuccess,
    [types.GET_WISHLIST_SUCCESS]: getProductsWishListSuccess,
    [types.ADD_WISHLISTS_SUCCESS]: addWishListsSuccess,
    [types.REMOVE_WISHLISTS_SUCCESS]: removeWishListsSuccess,
    [types.UPDATE_USER_SUCCESS]: updateUserSuccess,
    [types.GET_ADDRESSES_SUCCESS]: getAddressesSuccess,
    [types.GET_ORDER_SUCCESS]: getOrdersSuccess,
  },
  initialState
);
