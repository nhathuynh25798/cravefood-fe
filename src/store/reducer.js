import { combineReducers } from 'redux';

import product from './product/product.reducer';
import blog from './blog/blog.reducer';
import cart from './cart/cart.reducer';
import category from './category/category.reducer';
import page from './page/page.reducer';
import location from './location/location.reducer';
import globalValue from './globalValue/global.reducer';
import user from './user/user.reducer';

export default combineReducers({
  product,
  category,
  page,
  blog,
  cart,
  location,
  globalValue,
  user,
});
