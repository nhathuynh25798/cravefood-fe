import { takeLatest, put, call, all } from 'redux-saga/effects';

import { getBlogs, getBlogBySlug } from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';

import { types, actionCreators } from './blog.meta';

function* sagaFetchBlogs(action) {
  const { body, httpStatus } = yield call(getBlogs, action.payload);
  if (httpStatus === 200) {
    yield put(actionCreators.fetchBlogsSuccess(body));
  }
}

function* sagaFetchBlogBySlug(action) {
  const slug = action.payload;
  const { body, httpStatus } = yield call(getBlogBySlug, slug);
  if (httpStatus === 200) {
    yield put(actionCreators.fetchBlogBySlugSuccess(body));
  }
}

// Monitoring Sagas
function* blogMonitor() {
  yield all([
    takeLatest(types.FETCH_BLOGS, sagaErrorWrapper(sagaFetchBlogs)),
    takeLatest(types.FETCH_BLOG_BY_SLUG, sagaErrorWrapper(sagaFetchBlogBySlug)),
  ]);
}

export default blogMonitor;
