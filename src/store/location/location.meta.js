import { createAction } from 'redux-actions';

export const types = {
  FETCH_PROVINCES: 'FETCH_PROVINCES',
  FETCH_PROVINCES_SUCCESS: 'FETCH_PROVINCES_SUCCESS',
};

export const actionCreators = {
  actFetchProvinces: createAction(types.FETCH_PROVINCES),
  getProvincesSuccess: createAction(types.FETCH_PROVINCES_SUCCESS),
};
