import { all, fork } from 'redux-saga/effects';

import product from './product/product.saga';
import blogSaga from './blog/blog.saga';
import cartSaga from './cart/cart.saga';
import category from './category/category.saga';
import location from './location/location.saga';
import globalValue from './globalValue/global.saga';
import user from './user/user.saga';

export default function* rootSaga() {
  yield all([
    fork(product),
    fork(blogSaga),
    fork(cartSaga),
    fork(category),
    fork(location),
    fork(globalValue),
    fork(user),
  ]);
}
