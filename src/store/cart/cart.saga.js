import { takeLatest, put, all, call } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';

import { checkout } from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';
// import { actionCreator } from '../user/user.meta';
import { types, actionCreators } from './cart.meta';

function* sagaAddProductToCart(action) {
  const data = action.payload;
  yield put(actionCreators.actAddToCartSuccess(data));
}

function* sagaRemoveProductFromCart(action) {
  const data = action.payload;
  yield put(actionCreators.actRemoveProductFromCartSuccess(data));
  notification.success({
    message: 'Xóa món ăn thành công',
  });
}

function* sagaSelectAddress(action) {
  const data = action.payload;
  yield put(actionCreators.actSelectAddressSuccess(data));
  notification.success({
    message: 'Món ăn sẽ được giao tới địa chỉ này',
  });
}

function* sagaAddDeliveryDateAndNote(action) {
  const { deliveryDate, orderNote = null } = action.payload;
  yield put(
    actionCreators.actAddDeliveryDateAndNoteSuccess({
      orderNote: orderNote,
      deliveryDate: deliveryDate,
    })
  );

  notification.success({
    message: `Đơn hàng sẽ được giao vào ngày ${deliveryDate}`,
  });
}

function* sagaCheckout(action) {
  const data = action.payload;
  const { body } = yield call(checkout, data);

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreators.actCheckoutSuccess());

  // yield put(actionCreator.getAddress({ userId: data?.userId }));
  yield navigate('/');
}

// Monitoring Sagas
function* cartMonitor() {
  yield all([
    takeLatest(
      types.ADD_PRODUCT_TO_CART,
      sagaErrorWrapper(sagaAddProductToCart)
    ),
    takeLatest(types.SELECT_ADDRESS, sagaErrorWrapper(sagaSelectAddress)),
    takeLatest(
      types.ADD_DELIVERY_DATE_AND_NOTE,
      sagaErrorWrapper(sagaAddDeliveryDateAndNote)
    ),
    takeLatest(
      types.REMOVE_PRODUCT_FROM_CART,
      sagaErrorWrapper(sagaRemoveProductFromCart)
    ),
    takeLatest(types.CHECKOUT, sagaErrorWrapper(sagaCheckout)),
  ]);
}

export default cartMonitor;
