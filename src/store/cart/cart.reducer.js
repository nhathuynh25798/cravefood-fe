import { types } from './cart.meta';
import { handleActions } from 'redux-actions';
import { notification } from 'antd';
import moment from 'moment';

import { calculateSalePrice } from 'utils/helper';

const defaultCartState = {
  items: [],
  total: 0,
  cartTotalOriginalPrice: 0,
  cartTotalDiscountPrice: 0,
  deliveryAddress: null,
  deliveryDate: moment().add(1, 'days').format('DD/MM/YYYY'),
  orderNote: '',
};

const totalInArray = (array) => {
  return array.reduce((prev, curr) => prev + curr.quantity, 0);
};

const totalPrice = (array) => {
  return array.reduce(
    (prev, curr) => prev + curr?.quantity * curr?.dishSize?.price,
    0
  );
};

const cartTotalOriginalPrice = (array) => {
  return array.reduce((prev, curr) => prev + totalPrice(curr?.quantity), 0);
};

const cartTotalDiscountPrice = (array) => {
  return array.reduce(
    (prev, curr) =>
      prev +
      calculateSalePrice(curr?.product?.discount, totalPrice(curr?.quantity)),
    0
  );
};

const addProductToCartSuccess = (state, action) => {
  const cartItem = action.payload;
  const itemsClone = [...state.items];
  const index = itemsClone.findIndex(
    (x) => x.product.id === cartItem.product.id
  );
  if (index !== -1) {
    itemsClone[index].quantity.map(
      (item, index) =>
        (item.quantity = cartItem.quantity?.[index].quantity ?? 0)
    );
    notification.success({
      message: 'Cập nhật giỏ hàng thành công',
    });
  } else {
    itemsClone.push(cartItem);
    notification.success({
      message: 'Thêm giỏ hàng thành công',
    });
  }
  return {
    ...state,
    items: itemsClone,
    total: itemsClone.reduce(
      (prev, curr) => prev + totalInArray(curr.quantity),
      0
    ),
    cartTotalOriginalPrice: cartTotalOriginalPrice(itemsClone),
    cartTotalDiscountPrice: cartTotalDiscountPrice(itemsClone),
  };
};

const removeProductFromCartSuccess = (state, action) => {
  const cartItem = action.payload;
  const itemsClone = [...state.items];
  const index = itemsClone.findIndex((x) => x.product.id === cartItem);
  if (index !== -1) {
    itemsClone.splice(index, 1);
  }
  return {
    ...state,
    items: itemsClone,
    total: itemsClone.reduce(
      (prev, curr) => prev + totalInArray(curr.quantity),
      0
    ),
    cartTotalOriginalPrice: cartTotalOriginalPrice(itemsClone),
    cartTotalDiscountPrice: cartTotalDiscountPrice(itemsClone),
  };
};

const selectAddressSuccess = (state, action) => {
  return {
    ...state,
    deliveryAddress: action.payload,
  };
};

const addDeliveryDateAndNoteSuccess = (state, action) => {
  const {
    deliveryDate = moment().add(1, 'days').format('dd/MM/yyyy'),
    orderNote = '',
  } = action.payload;
  return {
    ...state,
    deliveryDate: deliveryDate,
    orderNote: orderNote,
  };
};

const checkoutSuccess = (state, action) => ({
  ...state,
  items: [],
  total: 0,
  cartTotalOriginalPrice: 0,
  cartTotalDiscountPrice: 0,
  deliveryDate: undefined,
  orderNote: '',
});

export default handleActions(
  {
    [types.ADD_PRODUCT_TO_CART_SUCCESS]: addProductToCartSuccess,
    [types.REMOVE_PRODUCT_FROM_CART_SUCCESS]: removeProductFromCartSuccess,
    [types.SELECT_ADDRESS_SUCCESS]: selectAddressSuccess,
    [types.ADD_DELIVERY_DATE_AND_NOTE_SUCCESS]: addDeliveryDateAndNoteSuccess,
    [types.CHECKOUT_SUCCESS]: checkoutSuccess,
  },
  defaultCartState
);
