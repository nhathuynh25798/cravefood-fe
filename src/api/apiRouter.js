import { buildRequest } from 'api';
import cookie from 'js-cookie';
import qs from 'qs';

// Product
export const getProducts = (params) =>
  params
    ? buildRequest(`/product/${qs.stringify(params)}`, {
        method: 'GET',
      }).request()
    : buildRequest(`/product/`, {
        method: 'GET',
      }).request();

export const getProductBySlug = (slug) =>
  buildRequest(`/product/${slug}`, { method: 'GET' }).request();

export const searchProduct = (keyword) =>
  buildRequest(`/product/find/${keyword}`, { method: 'GET' }).request();

// export const getRelatedProduct = (slug) =>
//   buildRequest(`/product/${slug}`, { method: 'GET' }).request();

// export const createRating = (data) =>
//   buildRequest(`/product/rating/${data?.productId}`).request({
//     method: 'POST',
//     data,
//   });

// Blog
export const getBlogs = (params) => {
  return params
    ? buildRequest(`/blog?${qs.stringify(params)}`, { method: 'GET' }).request()
    : buildRequest(`/blog`, { method: 'GET' }).request();
};
export const getBlogBySlug = (slug) =>
  buildRequest(`/blog/${slug}`, { method: 'GET' }).request();

// Category
export const getCategories = (params) => {
  return params
    ? buildRequest(`/category/${qs.stringify(params)}`, {
        method: 'GET',
      }).request()
    : buildRequest(`/category/`, { method: 'GET' }).request();
};

export const getCategoryBySlug = (slug) => {
  return slug
    ? buildRequest(`/category/${slug}`, {
        method: 'GET',
      }).request()
    : buildRequest(`/category/`, { method: 'GET' }).request();
};

export const getProductByCategorySlug = (slug) => {
  return buildRequest(`/category/${slug}/products`, {
    method: 'GET',
  }).request();
};

//Location

// ==== province ====
export const getProvinces = (params) => {
  return params
    ? buildRequest(`/province/${qs.stringify(params)}`, {
        method: 'GET',
      }).request()
    : buildRequest(`/province/`, { method: 'GET' }).request();
};

// ==== global ====
export const getGlobalValue = () => {
  return buildRequest(`/globalValue/`, { method: 'GET' }).request();
};

// =========== User Request ========
const token = cookie.get('token');

export const getWishList = (id) =>
  buildRequest(`/user/wishlists`, {
    headers: { token: token, uid: id },
  }).request({ method: 'POST' });

export const getOrders = (id) =>
  buildRequest(`/user/orders`, {
    headers: { token: token, uid: id },
  }).request({ method: 'POST' });

export const getAddresses = (id) =>
  buildRequest(`/user/addresses`, {
    headers: { token: token, uid: id },
  }).request({ method: 'POST' });

// address
export const addAddress = (data) =>
  buildRequest(`/user/addAddress`, {
    method: 'POST',
    headers: { token: token },
  }).request({ data: data });

export const deleteAddress = (id) =>
  buildRequest(`/user/deleteAddress/${id}`, {
    method: 'POST',
    headers: { token: token },
  }).request();

// wishlist
export const addWishList = (data) =>
  buildRequest(`/user/addWishList`, {
    method: 'POST',
    headers: { token: token },
  }).request({ data: data });

export const deleteWishList = (data) =>
  buildRequest(`/user/deleteWishList`, {
    headers: { token: token },
  }).request({ method: 'POST', data: data });

export const getUser = (id) =>
  buildRequest(`/user/getUser/${id}`, {
    headers: { token: token },
  }).request({ method: 'POST' });

export const updateUser = (data) =>
  buildRequest(`/user/update`, {
    headers: { token: token },
  }).request({ method: 'POST', data: data });

export const customerLogin = (data) =>
  buildRequest('/auth/login', { method: 'POST' }).request({ data: data });

export const customerRegister = (data) =>
  buildRequest('/auth/register', { method: 'POST' }).request({ data: data });

// export const updateCustomerPassword = ({ data }) =>
//   authRequest.request({
//     url: '/changePassword',
//     method: 'POST',
//     data,
//   });

// export const refreshUserToken = ({ refreshToken }) =>
//   authRequest.request({
//     url: '/refreshToken',
//     method: 'POST',
//     headers: {
//       Authorization: `Bearer ${refreshToken}`,
//     },
//   });

// Order
export const checkout = (data) =>
  buildRequest(`/user/addOrder`, {
    method: 'POST',
    headers: { token: token },
  }).request({ data: data });

// export const sendFaq = data =>
//   faqRequest.request({
//     method: 'POST',
//     data,
//   })

// ============= SaS Package calculate =======
