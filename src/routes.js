import React, { lazy } from 'react';
import { Link, Router, Redirect } from '@reach/router';
import { Result } from 'antd';

const HomePage = lazy(() => import('./views/HomePage'));
const ProductDetailPage = lazy(() => import('./views/ProductDetailPage'));
const CheckoutPage = lazy(() => import('./views/CheckoutPage'));
const LoginPage = lazy(() => import('./views/LoginPage'));
const RegisterPage = lazy(() => import('./views/RegisterPage'));
const ForgetPasswordPage = lazy(() => import('./views/ForgetPasswordPage'));
const SearchPage = lazy(() => import('./views/SearchPage'));
const AboutPage = lazy(() => import('./views/AboutPage'));
const ContactPage = lazy(() => import('./views/ContactPage'));
const CategoryPage = lazy(() => import('./views/CategoryPage'));
// Support
const DeliveryTimePage = lazy(() => import('./views/Support/DeliveryTimePage'));
const OrderingGuidePage = lazy(() =>
  import('./views/Support/OrderingGuidePage')
);
const PrivacyPolicyPage = lazy(() =>
  import('./views/Support/PrivacyPolicyPage')
);
const ReturnPolicyPage = lazy(() => import('./views/Support/ReturnPolicyPage'));
const TermOfUsePage = lazy(() => import('./views/Support/TermOfUsePage'));
const TransferInformationPage = lazy(() =>
  import('./views/Support/TransferInformationPage')
);

// == profile ==
const WishListPage = lazy(() => import('./views/Profile/WishListPage'));
const ProfilePage = lazy(() => import('./views/Profile/ProfilePage'));
const DeliveryAddressPage = lazy(() =>
  import('./views/Profile/DeliveryAddressPage')
);
const OrdersPage = lazy(() => import('./views/Profile/OrdersPage'));

const PageNotFound = () => (
  <Result
    status="404"
    title="404"
    subTitle="Hmm. Chúng tôi gặp khó khăn khi tìm trang web đó."
    extra={<Link to="/">Back Home</Link>}
  />
);

const Routes = () => {
  return (
    <Router
      style={{
        backgroundColor: '--var(tertiary)',
        width: '100%',
        maxWidth: '1920px',
      }}
    >
      <Redirect from="/" to="/trang-chu" noThrow />
      <HomePage path="trang-chu" />
      <CategoryPage path="mon-le" />
      <CategoryPage path="mon-le/:slug" />
      <CategoryPage path="combo" />
      <ProductDetailPage path="/mon-le/chi-tiet/:slug" />
      <LoginPage path="/dang-nhap" />
      <RegisterPage path="/dang-ky" />
      <ForgetPasswordPage path="/quen-mat-khau" />
      <AboutPage path="/ve-chung-toi" />
      <SearchPage path="/tim-kiem" />
      <ContactPage path="/lien-he" />
      <CheckoutPage path="/thanh-toan" />

      <DeliveryTimePage path="/ho-tro-khach-hang/thong-tin-giao-hang" />
      <OrderingGuidePage path="/ho-tro-khach-hang/huong-dan-dat-hang" />
      <PrivacyPolicyPage path="/ho-tro-khach-hang/chinh-sach-bao-mat" />
      <ReturnPolicyPage path="/ho-tro-khach-hang/chinh-sach-doi-tra" />
      <TermOfUsePage path="/ho-tro-khach-hang/dieu-khoan-su-dung" />
      <TransferInformationPage path="/ho-tro-khach-hang/thong-tin-chuyen-khoan" />

      <WishListPage path="/danh-sach-yeu-thich" />
      <ProfilePage path="/thong-tin-ca-nhan" />
      <DeliveryAddressPage path="/dia-chi-giao-hang" />
      <OrdersPage path="/danh-sach-don-hang" />
      <PageNotFound path="*" />
    </Router>
  );
};

export default Routes;
