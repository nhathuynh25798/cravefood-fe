import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import { globalHistory } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import { actionCreators as productActions } from '../../store/product/product.meta';
import { actionCreators } from '../../store/cart/cart.meta';
import FloatingCart from '../../components/FloatingCart';
import ProductPurchase from './components/ProductPurchase';
import ImageMain from './components/ImageMain';
import ProductIntroduce from './components/ProductIntroduce';
import ProductDescription from './components/ProductDescription';

const ProductDetailPage = () => {
  const dispatch = useDispatch();
  const params = globalHistory.location;
  const paramsArray = params.pathname.split('/');
  const position = paramsArray.length - 1;
  const slug = paramsArray[position];

  const scrollToTop = () => {
    window.scrollTo({ top: 0 });
  };

  const product = useSelector((store) => store?.product?.bySlug);

  const fetchProductBySlug = () => {
    dispatch(productActions.actFetchProductBySlug({ slug: slug }));
  };

  useEffect(() => {
    fetchProductBySlug();
  }, [slug]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    scrollToTop();
  }, [slug]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleAddProductToCart = ({ product, quantity }) => {
    dispatch(actionCreators.actAddToCart({ product, quantity }));
  };

  return (
    <Row justify="center" align="middle" style={{ padding: '4rem 0' }}>
      <FloatingCart />
      <Col xs={{ span: 22, offset: 1 }} md={{ span: 21, offset: 1.5 }}>
        <Row justify="space-between">
          <Col
            xs={{ span: 24 }}
            md={{ span: 8 }}
            style={{ padding: '0 0.8rem' }}
          >
            <ImageMain data={product} />
          </Col>
          <Col
            xs={{ span: 24 }}
            md={{ span: 9 }}
            style={{ padding: '0 0.8rem' }}
          >
            <ProductIntroduce data={product} />
          </Col>
          <Col
            xs={{ span: 24 }}
            md={{ span: 7 }}
            style={{ padding: '0 0.8rem' }}
          >
            <ProductPurchase
              data={product}
              onAddToCart={handleAddProductToCart}
            />
          </Col>
          <Col xs={{ span: 24 }} style={{ padding: '0 0.8rem' }}>
            <ProductDescription data={product} />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default ProductDetailPage;
