import React from 'react';
import { Row, Col } from 'antd';

const FooterProductDescription = () => {
  return (
    <Row>
      <Col xs={{ span: 24 }}>
        <div>
          <h3
            style={{ fontSize: '2rem', fontWeight: 'bold', color: '#e03e2d' }}
          >
            Hộp giữ nhiệt CraveFood:
          </h3>
          <ul
            style={{
              fontSize: '1.6rem',
              listStyleType: 'initial',
              padding: '2.4rem',
            }}
          >
            <li>
              Làm bằng giấy giấy Kraft tự hủy của Nhật Bản, hộp đựng đồ của
              CraveFood không chỉ thân thiện với môi trường, mà còn giữ được độ
              nóng và mùi vị thơm ngon cho món ăn.
            </li>
            <li>
              Hình dáng hộp đa dạng, riêng biệt theo đặc thù của từng món. Ngoài
              ra, các lỗ thông hơi được sắp xếp hợp lý nhằm giữ độ giòn cho các
              món chiên... Tuy làm bằng giấy, hộp vẫn đủ độ cứng cáp và chắc
              chắn.
            </li>
            <li>
              Hộp đựng thức ăn của CraveFood đảm bảo giữ được độ nóng giòn cho
              các món ăn (gà bó xôi, cá tai tượng chiên xù…); giữ nguyên hình
              dáng cho món heo sữa quay da giòn; hộp đựng bắt mắt dành cho những
              món cua sang trọng. Công ty còn có hộp chuyên dụng cho món cá lóc
              quay me, giò heo chiên giòn……..
            </li>
          </ul>
          <img src="/assets/images/product-box.jpg" alt="product-box" />
        </div>
      </Col>
    </Row>
  );
};

export default FooterProductDescription;
