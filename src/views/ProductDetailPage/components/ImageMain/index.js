import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';

import './style.scss';

const ImageMain = (props) => {
  const { data } = props;
  require('dotenv').config();

  const [mainImage, setMainImage] = useState(data?.images?.[0]?.content);
  const [backgroundPosition, setBackgroundPosition] = useState('0% 0%');

  useEffect(() => {
    setMainImage(data?.images?.[0]?.content);
  }, [data]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleMouseMove = (e) => {
    const { left, top, width, height } = e.target.getBoundingClientRect();
    const x = ((e.pageX - left) / width) * 100;
    const y = ((e.pageY - top) / height) * 100;
    setBackgroundPosition(`${x}% ${y}%`);
  };

  return (
    <>
      <Row
        style={{ padding: '0.8rem 0', width: '100%' }}
        className="image-zoom"
      >
        <figure
          onMouseMove={handleMouseMove}
          style={{
            backgroundImage: `url("${
              process.env.REACT_APP_BACKEND_URL + mainImage
            }")`,
            backgroundPosition: `${backgroundPosition}`,
          }}
        >
          <img
            src={process.env.REACT_APP_BACKEND_URL + mainImage}
            alt="main-zoom"
          />
        </figure>
      </Row>
      {(data?.discount || data?.discount !== 0) && (
        <div className="product-card__discount">
          <span className="product-card__discount--content">
            -{data?.discount * 100}%
          </span>
        </div>
      )}
      <Row gutter={[8, 8]} style={{ overflow: 'auto', flexWrap: 'nowrap' }}>
        {data?.images?.sort()?.map((item, index) => (
          <Col md={{ span: 6 }} key={index}>
            <img
              onClick={() => setMainImage(item?.content)}
              key={index}
              src={process.env.REACT_APP_BACKEND_URL + item?.content}
              alt="product"
              style={{
                width: '100%',
                height: '6rem',
                maxHeight: '7rem',
                border:
                  item === mainImage ? '2px solid var(--primary)' : 'none',
                borderRadius: '0.4rem',
                cursor: 'pointer',
              }}
            />
          </Col>
        ))}
      </Row>
    </>
  );
};
export default ImageMain;
