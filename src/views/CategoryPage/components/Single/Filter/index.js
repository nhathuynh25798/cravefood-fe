import React, { useState } from 'react';
import { Row, Col, Select } from 'antd';
import { navigate } from '@reach/router';
import { useSelector } from 'react-redux';
import './style.scss';
// const { Search } = Input;
const { Option } = Select;

export const defaultSearchFunction = (input, option) =>
  option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

const Filter = (props) => {
  const { categories = [] } = useSelector((store) => store.category);
  const { location } = props;

  const [seletValue, setSelectValue] = useState(
    ['mon-le', '/mon-le', '/mon-le/', 'mon-le/'].includes(location)
      ? 'Tất cả'
      : location
  );

  return (
    <Col className="category-filter-container">
      <Row
        className="category-filter-option-container"
        type="flex"
        justify="space-between"
        gutter={[16, 16]}
      >
        <Col
          className="category-filter-group"
          xs={24}
          md={7}
          style={{ minWidth: 'min-content' }}
        >
          <Row
            type="flex"
            justify="space-between"
            align="middle"
            style={{ width: '100%' }}
          >
            <Col xs={6} md={6}>
              <span className="option-title">Danh mục</span>
            </Col>
            <Col xs={16} md={16}>
              <Select
                className="option-select-single"
                defaultValue={seletValue}
                showSearch
                filterOption={defaultSearchFunction}
                onChange={(val) => {
                  if (!!val) {
                    setSelectValue(val);
                    navigate(`/mon-le/${val}`);
                  } else {
                    setSelectValue('Tất cả');
                    navigate(`/mon-le`);
                  }
                }}
              >
                <Option value={''}>Tất cả</Option>
                {categories.map((cate) => (
                  <Option key={cate?.id} value={cate?.slug}>
                    {cate?.name}
                  </Option>
                ))}
              </Select>
            </Col>
          </Row>
        </Col>
        {/* <Col className="category-filter-group" xs={24} md={8}>
          <Row type="flex" justify="end" style={{ width: '100%' }}>
            <Col xs={24} md={22} lg={18}>
              <Search
                size="medium"
                enterButton
                onSearch={(e) => navigate(`/search?keyword=${e}`)}
              />
            </Col>
          </Row>
        </Col> */}
      </Row>
    </Col>
  );
};

export default Filter;
