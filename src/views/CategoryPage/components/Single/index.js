import React, { useState, useEffect } from 'react';
import { Col, Row } from 'antd';
import { useSelector, useDispatch } from 'react-redux';

import { actionCreators } from '../../../../store/category/category.meta';
import CategoryCart from '../Cart';
import Filter from './Filter';
import CategoriesList from './CategoriesList';
import CategorySingle from './CategorySingle';
import './style.scss';

const Single = (props) => {
  const dispatch = useDispatch();
  const { categories = [] } = useSelector((store) => store.category);
  const { location, locations } = props;
  const [active, setActive] = useState(location);

  const getParams = (loca) => {
    const arr = loca.split('/');
    return arr[arr.length - 1];
  };

  const fetchAllCategories = () => {
    dispatch(actionCreators.actFetchCategories({}));
  };

  useEffect(() => {
    fetchAllCategories();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setActive(location);
  }, [location]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Col xs={24}>
      <Filter location={getParams(active)} />
      <Row
        gutter={{ md: 24, lg: 24 }}
        type="flex"
        justify="space-between"
        style={{ maxWidth: '100%', marginLeft: 0 }}
        className="custom-gutter"
      >
        <Col xs={0} xl={17} style={{ padding: 0 }}>
          {['mon-le', '/mon-le', '/mon-le/', 'mon-le/'].includes(active) ? (
            <CategoriesList categories={categories} />
          ) : (
            <CategorySingle
              location={getParams(active)}
              locations={locations}
            />
          )}
        </Col>
        <Col
          xs={0}
          xl={7}
          style={{ paddingTop: 45, paddingLeft: 8, paddingRight: 0 }}
        >
          <div
            style={{
              border: '1px solid #DDDDDD',
              boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.05)',
              borderRadius: 4,
            }}
          >
            <CategoryCart />
          </div>
        </Col>
      </Row>
    </Col>
  );
};

export default Single;
