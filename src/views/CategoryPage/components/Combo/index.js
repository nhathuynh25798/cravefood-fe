import React, { useEffect } from 'react';
import { Col, Row } from 'antd';
// import { useDispatch, useSelector } from 'react-redux';
// import { actionCreator } from 'store/product/product.meta'
import Filter from './Filter';
import Promotion from '../Single/Promotion';
import ProductList from './ProductList';
import CategoryCart from '../Cart';

const Combo = () => {
  //   const dispatch = useDispatch()
  //   const { combos } = useSelector((store) => store.product)

  //   useEffect(() => {
  //     if (!combos || combos?.length === 0)
  //       dispatch(actionCreator.getComboCategory())
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //   }, [])

  return (
    <Col xs={24}>
      <Promotion />
      <Filter />
      <Row gutter={16} type="flex" style={{ maxWidth: '100%', marginLeft: 0 }}>
        <Col xs={24} xl={16} style={{ paddingLeft: 0 }}>
          <ProductList />
        </Col>
        <Col
          xs={0}
          xl={8}
          style={{ paddingTop: 80, paddingLeft: 8, paddingRight: 0 }}
        >
          <div
            style={{
              border: '1px solid #DDDDDD',
              boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.05)',
              borderRadius: 4,
            }}
          >
            <CategoryCart />
          </div>
        </Col>
      </Row>
    </Col>
  );
};

export default Combo;
