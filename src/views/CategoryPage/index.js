import React, { useState, useEffect } from 'react';
import { Row, Col, Typography } from 'antd';
import { Link } from '@reach/router';

import Single from './components/Single';

import 'antd/dist/antd.css';
import './style.scss';

const { Title } = Typography;

const CategoryPage = (props) => {
  const [active, setActive] = useState(
    props.location.pathname.includes('combo') ? 'combo' : 'mon-le'
  );

  useEffect(() => {
    setActive(props.location.pathname.includes('combo') ? 'combo' : 'mon-le');
  }, [props.location.pathname]);

  return (
    <Row className="category-container" justify="center" align="middle">
      <Col md={20}>
        <Row>
          <Col md={24}>
            <Title level={2}>Món ăn giao tận nơi</Title>
          </Col>
          <Col md={24}>
            <Row gutter={{ md: 16 }}>
              <Col md={12} className="category__tabbar">
                <Link
                  className="category__link"
                  style={{ display: 'inline-block', width: '100%' }}
                  to="/mon-le"
                >
                  <div
                    className="category__link--div"
                    style={{
                      backgroundColor:
                        active === 'mon-le'
                          ? 'var(--secondary)'
                          : 'var(--quaternary)',
                      color:
                        active === 'mon-le'
                          ? 'var(--quaternary)'
                          : 'var(--secondary)',
                    }}
                  >
                    Món lẻ
                  </div>
                </Link>
              </Col>
              <Col md={12} className="category__tabbar">
                <Link
                  className="category__link"
                  style={{ display: 'inline-block', width: '100%' }}
                  to="/combo"
                >
                  <div
                    className="category__link--div"
                    style={{
                      backgroundColor:
                        active === 'combo'
                          ? 'var(--secondary)'
                          : 'var(--quaternary)',
                      color:
                        active === 'combo'
                          ? 'var(--quaternary)'
                          : 'var(--secondary)',
                    }}
                  >
                    <img
                      src="/assets/images/combo-icon.png"
                      style={{
                        width: '3.2rem',
                        height: '2rem',
                        marginRight: '1.2rem',
                      }}
                      alt="combo-icon"
                    />
                    Combo
                  </div>
                </Link>
              </Col>
            </Row>
          </Col>
          <Col md={24}>
            <Row>
              {active === 'mon-le' && (
                <Single
                  location={props.location.pathname}
                  locations={props.location}
                />
              )}
              {/* {active === 'combo' && <Combo />} */}
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default CategoryPage;
