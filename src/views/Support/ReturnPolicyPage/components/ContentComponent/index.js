import React from 'react';
import { Row, Col } from 'antd';

const ContentComponent = () => {
  const content =
    '<div><div><strong>I. Giới thiệu chương tr&igrave;nh</strong></div><div>&nbsp;</div><div>Nhằm n&acirc;ng cao chất lượng m&oacute;n ăn hơn nữa v&agrave; đem lại lợi &iacute;ch cao nhất cho người ti&ecirc;u d&ugrave;ng CraveFood thực hiện chương tr&igrave;nh&nbsp;<strong>&ldquo;1 ĐỔI 1&rdquo;</strong>&nbsp;d&agrave;nh cho c&aacute;c kh&aacute;ch h&agrave;ng sử dụng sản phẩm, dịch vụ của CraveFood đ&atilde; ra đời kể từ ng&agrave;y 17/03/2015.</div><div>&nbsp;</div></div><div><div><strong>II. Thể lệ</strong></div><div>&nbsp;</div><div>Thể lệ chương tr&igrave;nh được quy định cụ thể ở c&aacute;c điều khoản sau như sau:</div><div>&nbsp;</div><div><strong>Điều 1.</strong>&nbsp;Chương tr&igrave;nh &aacute;p dụng cho tất cả c&aacute;c kh&aacute;ch h&agrave;ng sử dụng sản phẩm của CraveFood (sử dụng tại nh&agrave; h&agrave;ng hoặc giao h&agrave;ng tận nơi)</div><div>&nbsp;</div><div><strong>Điều 2.</strong>&nbsp;Khi kh&aacute;ch h&agrave;ng, ph&aacute;t hiện sản phẩm của CraveFood kh&ocirc;ng đạt được c&aacute;c ti&ecirc;u chuẩn chất lượng v&agrave; định lượng được m&ocirc; tả trong phần chi tiết c&aacute;c m&oacute;n ăn tr&ecirc;n website cravefood.netlify.app v&agrave; cảm thấy kh&ocirc;ng h&agrave;i l&ograve;ng về điều đ&oacute;. Qu&yacute; kh&aacute;ch phản &aacute;nh với Bộ phận Chăm S&oacute;c Kh&aacute;ch H&agrave;ng của CraveFood theo số điện thoại: 0343 661 688</div><div>&nbsp;</div><div><strong>Điều 3.</strong>&nbsp;C&aacute;c m&oacute;n ăn của CraveFood kh&ocirc;ng đạt được hơn 80% c&aacute;c ti&ecirc;u chuẩn chất lượng v&agrave; định lượng của CraveFood đ&atilde; c&ocirc;ng bố th&igrave; kh&aacute;ch h&agrave;ng sẽ được bồi ho&agrave;n 1 đổi 1. Kh&aacute;ch h&agrave;ng kh&ocirc;ng phải b&ugrave; th&ecirc;m bất cứ chi ph&iacute; n&agrave;o.</div><div>&nbsp;</div><div><strong>Điều 4.</strong>&nbsp;CraveFood nhận phản hồi kh&aacute;ch h&agrave;ng chậm nhất l&agrave; 60 ph&uacute;t kể từ l&uacute;c qu&yacute; kh&aacute;ch nhận được h&agrave;ng</div><div>&nbsp;</div><div><strong>Điều 5.</strong>&nbsp;Qu&yacute; kh&aacute;ch đảm bảo giữ được hiện trường về lỗi sản phầm để chứng minh h&agrave;ng kh&ocirc;ng đạt ti&ecirc;u chuẩn về chất lượng để đối chứng khi qu&yacute; kh&aacute;ch nhận lại h&agrave;ng đổi mới với nh&acirc;n vi&ecirc;n CraveFood.</div><div>&nbsp;</div><div><strong>Điều 6.</strong>&nbsp;Chương tr&igrave;nh 1 đổi 1 khẳng định uy t&iacute;n v&agrave; chất lượng sản phẩm của CraveFood c&ugrave;ng với c&aacute;i t&acirc;m của doanh nghiệp c&ugrave;ng với to&agrave;n thể nh&acirc;n viện của CraveFood. Sự sai x&oacute;t trong việc chế biến l&agrave;m cho sản phẩm bị lỗi l&agrave; rất đ&aacute;ng tiếc, kh&ocirc;ng mong muốn. CraveFood đổi h&agrave;ng để chứng minh t&acirc;m huyết của m&igrave;nh, CraveFood thật sự kh&ocirc;ng mong muốn c&oacute; những trường hợp ti&ecirc;u cực, cố &yacute; l&agrave;m sai lệch &yacute; nghĩa của chương tr&igrave;nh l&agrave;m kh&oacute; cho CraveFood.</div><div>&nbsp;</div><div><strong>Điều 7.</strong>&nbsp;CraveFood kh&ocirc;ng c&oacute; ch&iacute;nh s&aacute;ch ho&agrave;n tiền đối với tất cả sản phẩm.</div></div><div style="text-align: right;"><strong>cravefood</strong></div>';
  return (
    <Row>
      <Col xs={{ span: 24 }}>
        <div
          style={{ fontSize: '1.6rem' }}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </Col>
    </Row>
  );
};
export default ContentComponent;
