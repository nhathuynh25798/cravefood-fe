import React from 'react'
import { Row, Col } from 'antd'

const ContentComponent = () => {
  const content =
    '<div><strong>Với dịch vụ thanh to&aacute;n online bằng c&aacute;ch chuyển khoản trước thế n&agrave;y, bạn sẽ chủ động hơn trong việc nhận c&aacute;c m&oacute;n ngon của CraveFood m&agrave; kh&ocirc;ng phải trả bằng tiền mặt phức tạp. Hoặc nếu bạn muốn tặng những m&oacute;n đồ ăn cho người th&acirc;n, bạn b&egrave; th&igrave; việc chuyển khoản trước thế n&agrave;y v&ocirc; c&ugrave;ng cần thiết v&agrave; tiện lợi.</strong></div><div>Cập nhật th&ocirc;ng tin chuyển khoản của <strong>CraveFood</strong> để việc thanh to&aacute;n online trở n&ecirc;n thật đơn giản, nhanh gọn v&agrave; tiện lợi nhất bạn nh&eacute;. Với dịch vụ thanh to&aacute;n online bằng c&aacute;ch chuyển khoản trước thế n&agrave;y, bạn sẽ chủ động hơn trong việc nhận c&aacute;c m&oacute;n ngon của CraveFood m&agrave; kh&ocirc;ng phải trả bằng tiền mặt phức tạp. Hoặc nếu bạn muốn tặng những m&oacute;n đồ ăn cho người th&acirc;n, bạn b&egrave; th&igrave; việc chuyển khoản trước thế n&agrave;y v&ocirc; c&ugrave;ng cần thiết v&agrave; tiện lợi.</div><div><div>&nbsp;</div><div><span style="text-decoration: underline;"><strong>TH&Ocirc;NG TIN CHUYỂN KHOẢN:</strong></span></div><div>D&agrave;nh cho kh&aacute;ch h&agrave;ng muốn thanh to&aacute;n online:</div><div>T&ecirc;n t&agrave;i khoản :&nbsp;<strong>HUYNH TRONG NHAT</strong></div><div>Số t&agrave;i khoản :&nbsp;<strong>3141 000 2393 475</strong>&nbsp;-&nbsp;<strong>Ng&acirc;n H&agrave;ng BIDV</strong></div><div></div></div><div><strong><div>&nbsp;</div></div><div><strong>CraveFood hi vọng với sự tiện lợi trong việc thanh to&aacute;n online n&agrave;y, qu&yacute; kh&aacute;ch sẽ c&oacute; những bữa ăn tại nh&agrave;, d&agrave;nh tặng người th&acirc;n thật ngon miệng v&agrave; &yacute; nghĩa nhất.</strong></div><div><div><h4 style="text-align: center;"><span style="color: red; font-size: 2rem;">GỌI NGAY: 0343 661 688</span></h4></div><h4 style="text-align: center;"><span style="color: var(--primary); font-size: 3rem;"><em>CraveFood - PHÁT HUY TRUYỀN THỐNG - KẾT NỐI HIỆN ĐẠI!</em></span></h4></div></div><div style="text-align: right;"><strong>cravefood</strong></div>'
  return (
    <Row>
      <Col xs={{ span: 24 }}>
        <div
          style={{ fontSize: '1.6rem' }}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </Col>
    </Row>
  )
}
export default ContentComponent
