import React from 'react';
import { Row, Col } from 'antd';

const ContentComponent = () => {
  const content =
    '<div><div><div>Đặt h&agrave;ng qua điện thoại v&agrave; đặt h&agrave;ng online qu&yacute; kh&aacute;ch vui l&ograve;ng đặt h&agrave;ng trước 60 &ndash; 120 ph&uacute;t v&agrave;o ng&agrave;y thường. V&agrave;o cuối tuần qu&yacute; kh&aacute;ch đặt trước &iacute;t nhất 2 - 3 tiếng để được CraveFood phục vụ tốt nhất.</div><div>&nbsp;</div><div><div>QUY TR&Igrave;NH HƯỚNG DẪN ĐẶT H&Agrave;NG ONLINE TẠI WEBSITE CRAVEFOOD.NETLIFY.APP</div><div>M&ocirc; tả c&aacute;c bước đơn giản khi đặt h&agrave;ng tại CraveFood.</div><div>&nbsp;</div></div><div>Bạn c&oacute; thể lựa chọn một trong những c&aacute;ch mua h&agrave;ng sau:</div><div>&nbsp;</div><div><strong>1. Đặt h&agrave;ng qua điện thoại:</strong> Qu&yacute; kh&aacute;ch đặt h&agrave;ng trước 60 &ndash; 120 ph&uacute;t v&agrave;o ng&agrave;y thường. V&agrave;o cuối tuần qu&yacute; kh&aacute;ch đặt trước &iacute;t nhất 2-3 tiếng để được Flyfood phục vụ tốt nhất. <span style="color: var(--primary)"><strong>Hotline : 0343 661 688</strong></span></div><div>&nbsp;</div><div><strong>2. Đặt mua h&agrave;ng online:</strong> Sau khi nhập đầy đủ th&ocirc;ng tin của qu&yacute; kh&aacute;ch, qu&yacute; kh&aacute;ch Click v&agrave;o n&uacute;t "X&aacute;c nhận đơn h&agrave;ng" Hệ thống của CraveFood sẽ lưu lại đơn h&agrave;ng của qu&yacute; kh&aacute;ch để xử l&yacute; v&agrave; chuyển đến nh&acirc;n vi&ecirc;n của CraveFood, đồng thời CraveFood sẽ gửi th&ocirc;ng tin đơn h&agrave;ng v&agrave;o email của qu&yacute; kh&aacute;ch. Sau qu&aacute; tr&igrave;nh xử l&yacute; đ&oacute;, hệ thống sẽ chuyển đến trang "Th&ocirc;ng Tin Đơn H&agrave;ng" để qu&yacute; kh&aacute;ch c&oacute; thể xem lại tất cả th&ocirc;ng tin đơn h&agrave;ng của qu&yacute; kh&aacute;ch. Tại th&ocirc;ng tin đơn h&agrave;ng, qu&yacute; kh&aacute;ch c&oacute; thể hủy đơn h&agrave;ng bằng c&aacute;ch click v&agrave;o n&uacute;t "Hủy".</div><div>&nbsp;</div><div>Sau khi nhận được đơn h&agrave;ng từ qu&yacute; kh&aacute;ch, nh&acirc;n vi&ecirc;n của CraveFood sẽ li&ecirc;n hệ theo th&ocirc;ng tin trong đơn h&agrave;ng m&agrave; qu&yacute; kh&aacute;ch đ&atilde; cung cấp để x&aacute;c nhận đơn h&agrave;ng từ qu&yacute; kh&aacute;ch.</div><div>&nbsp;</div><div>Sau khi x&aacute;c nhận th&agrave;nh c&ocirc;ng CraveFood sẽ tiến h&agrave;nh giao h&agrave;ng đến tận nơi cho qu&yacute; kh&aacute;ch theo đ&uacute;ng thời gian quy định trong đơn h&agrave;ng.</div></div></div><div style="text-align: right;"><strong>cravefood</strong></div>';
  return (
    <Row>
      <Col xs={{ span: 24 }}>
        <div
          style={{ fontSize: '1.6rem' }}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </Col>
    </Row>
  );
};
export default ContentComponent;
