/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from 'react';
import { Row, Col, Steps, Button, Modal, notification } from 'antd';
import { navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import { actionCreator } from 'store/user/user.meta';
import { actionCreators } from 'store/cart/cart.meta';
import { sleep } from 'utils';
import AddressCard from 'components/AddressCard';
import AddressForm from 'components/AddressForm';
import CheckoutStep2 from './components/CheckoutStep2';
import CheckoutStep3 from './components/CheckoutStep3';
import CheckoutStep4 from './components/CheckoutStep4';

import 'antd/dist/antd.css';
import './style.scss';

const AddressFormFinish = (props) => {
  const dispatch = useDispatch();
  const { deliveryAddress } = useSelector((store) => store.cart);

  const { data } = props;

  const [selected, setSelected] = useState(
    deliveryAddress?.id ?? data?.[0]?.id
  );
  const [visible, setVisible] = useState(false);

  const showModal = () => {
    setVisible(!visible);
  };

  const handleSelected = (selected) => {
    setSelected(selected);
  };

  const autoSelectAddress = () => {
    if (!!!deliveryAddress) {
      dispatch(actionCreators.actSelectAddress(data?.[0]));
      setSelected(data?.[0]?.id);
    }
  };

  useEffect(() => {
    autoSelectAddress();
  }, [selected]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Row justify="space-between" gutter={[24, 24]}>
      <Col xs={24} sm={24}>
        <Row justify="space-between">
          <Col xs={12} sm={12}>
            <h2
              style={{
                fontSize: '2.4rem',
                fontWeight: '600',
                lineHeight: '3rem',
              }}
            >
              Thông tin giao hàng
            </h2>
          </Col>
          <Col xs={12} sm={12}>
            <Row justify="end">
              <Button size="large" type="primary" onClick={showModal}>
                Thêm địa chỉ mới
              </Button>
              <Modal
                className="address-modal"
                visible={visible}
                onCancel={() => setVisible(false)}
              >
                <AddressForm onFinish={(values) => setVisible(values)} />
              </Modal>
            </Row>
          </Col>
        </Row>
      </Col>
      {data?.map((item) => (
        <Col xs={24} sm={12} key={item?.id}>
          <AddressCard
            address={item}
            selected={selected}
            onSelected={handleSelected}
          />
        </Col>
      ))}
    </Row>
  );
};

const CheckoutPage = () => {
  const dispatch = useDispatch();
  const { Step } = Steps;
  const { user = {}, addresses = [] } = useSelector((store) => store.user);
  const [current, setCurrent] = useState(0);
  // eslint-disable-next-line
  const [disabled, setDisabled] = useState(!addresses?.[0]?.id);

  const fetchAddresses = (id) => {
    dispatch(actionCreator.getAddress({ userId: id }));
  };

  useEffect(() => {
    if (!user?.id) {
      notification.warning({
        message: 'Vui lòng đăng nhập trước khi thanh toán!',
      });
      sleep(500);
      navigate('/dang-nhap');
    } else {
      fetchAddresses(user?.id);
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  const {
    total,
    deliveryAddress,
    deliveryDate,
    orderNote,
    cartTotalDiscountPrice,
    cartTotalOriginalPrice,
    items,
  } = useSelector((store) => store.cart);

  const createOrderCode = () => {
    return (
      'DH_' +
      moment().format('DDMMYYYY') +
      '_' +
      Math.random().toString(36).substr(2, 9)
    );
  };

  const createQuantityItem = (item = {}) => {
    const { quantity } = item;
    const listItems = [];
    quantity
      ?.filter((qt) => qt?.quantity > 0)
      ?.map((it) =>
        listItems.push({
          productId: it?.dishSize?.productId,
          sizeId: it?.dishSize?.sizeId,
          quantity: it?.quantity,
        })
      );
    return listItems;
  };

  const createOrderItem = (items = []) => {
    const newOrderItems = [];
    items?.map((it) => newOrderItems.push(createQuantityItem(it)));
    return newOrderItems.flat();
  };

  const createOrder = () => {
    const codeString = createOrderCode();

    const {
      userId,
      name,
      detail,
      ward,
      district,
      province,
      phone,
      note,
    } = deliveryAddress;

    return {
      userId: userId,
      name: name,
      detail: detail,
      ward: ward,
      district: district,
      province: province,
      phone: phone,
      deliveryDate: deliveryDate,
      codeString: codeString,
      note: orderNote,
      addressNote: note,
      total: total,
      enable: 1,
      status: 0,
      cartTotalOriginalPrice: cartTotalOriginalPrice + 39000,
      cartTotalDiscountPrice: cartTotalDiscountPrice + 39000,
      items: createOrderItem(items),
    };
  };

  const handleFinishOrder = () => {
    dispatch(actionCreators.actCheckout(createOrder()));
  };

  const handleFinish = (disabled) => {
    setDisabled(disabled);
  };

  const handleCurrentChange = (current) => {
    setCurrent(current);
  };

  const steps = [
    {
      title: 'Địa chỉ giao hàng',
      content: !!!addresses.length ? (
        <AddressForm onFinish={handleFinish} />
      ) : (
        <AddressFormFinish data={addresses} />
      ),
    },
    {
      title: 'Kiểm tra đơn hàng',
      content: <CheckoutStep2 />,
    },
    {
      title: 'Chọn ngày giao hàng',
      content: <CheckoutStep3 />,
    },
    {
      title: 'Xác nhận & mua hàng',
      content: <CheckoutStep4 onCurrentChange={handleCurrentChange} />,
    },
  ];

  return (
    <Row
      justify="center"
      style={{
        margin: '2.4rem 0',
        width: '100%',
        minHeight: '80vh',
      }}
    >
      <Col
        xs={{ span: 24 }}
        md={{ span: 22 }}
        lg={{ span: 22 }}
        xl={{ span: 20 }}
      >
        <Row style={{ flex: '1 1 0%' }}>
          <Col xs={{ span: 0 }} md={{ span: 24 }}>
            <Steps
              current={current}
              className="checkout-steps"
              type="navigation"
            >
              {steps.map((item) => (
                <Step key={item?.title} title={item?.title} />
              ))}
            </Steps>
          </Col>

          <Col
            xs={{ span: 24 }}
            style={{ marginTop: '3.2rem' }}
            className="steps-content"
          >
            {steps[current].content}
          </Col>

          <Col xs={{ span: 24 }} className="steps-action">
            {current === steps.length - 1 && (
              <Button
                type="primary"
                size="large"
                onClick={handleFinishOrder}
                className="steps-action__button steps-action__button--next"
              >
                Hoàn thành
              </Button>
            )}

            {current < steps.length - 1 && (
              <Button
                type="primary"
                size="large"
                disabled={disabled}
                onClick={() => {
                  const newCurrent = current + 1;
                  setCurrent(newCurrent);
                }}
                className="steps-action__button steps-action__button--next"
              >
                <div>
                  <span>Tiếp tục</span>
                  <img src="/assets/images/arrow-next-white.png" alt="next" />
                </div>
              </Button>
            )}

            {current > 0 && (
              <Button
                size="large"
                style={{ margin: '0 8px' }}
                onClick={() => {
                  const newCurrent = current - 1;
                  setCurrent(newCurrent);
                }}
                className="steps-action__button steps-action__button--prev"
              >
                <div>
                  <img
                    src="/assets/images/arrow-left-green.png"
                    alt="previous"
                  />
                  <span>Quay lại</span>
                </div>
              </Button>
            )}
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default CheckoutPage;
