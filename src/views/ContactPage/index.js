import React from 'react';
import { Row, Col } from 'antd';

import ContactForm from './components/ContactForm';
import './style.scss';

const ContactPage = () => {
  return (
    <Row
      justify="center"
      align="center"
      className="contact__wrapper"
      style={{
        backgroundImage: 'url("/assets/images/contact.jpg")',
      }}
    >
      <Col md={{ span: 12 }} className="contact__form">
        <ContactForm />
      </Col>
    </Row>
  );
};
export default ContactPage;
