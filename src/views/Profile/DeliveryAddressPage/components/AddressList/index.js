import React, { useEffect, useState } from 'react';
import { Row, Col, Button, Modal, Empty } from 'antd';
import { navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import { actionCreator } from 'store/user/user.meta';
import AddressCard from 'components/AddressCard';
import AddressForm from 'components/AddressForm';

const AddressList = () => {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);

  const showModal = () => {
    setVisible(!visible);
  };

  const { user = {}, addresses = [] } = useSelector((store) => store.user);

  const fetchAddresses = (id) => {
    dispatch(actionCreator.getAddress({ userId: id }));
  };

  useEffect(() => {
    if (!user) {
      navigate('/');
    } else {
      fetchAddresses(user?.id);
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  const NewAddressButton = (props) => (
    <Col xs={24} sm={24}>
      <Row justify={props?.justify ?? 'end'}>
        <Button size="large" type="ghost" onClick={showModal}>
          Thêm địa chỉ mới
        </Button>
        <Modal
          className="address-modal"
          visible={visible}
          onCancel={() => setVisible(false)}
        >
          <AddressForm onFinish={(values) => setVisible(values)} />
        </Modal>
      </Row>
    </Col>
  );

  return (
    <Row justify="start" gutter={[16, 16]}>
      {addresses?.length > 0 ? (
        <>
          <NewAddressButton />
          {addresses
            ?.sort((a, b) => b?.defaultAddressValue - a?.defaultAddressValue)
            ?.map((it, idx) => (
              <Col xs={24} sm={24} key={idx}>
                <AddressCard address={it} showSelected={false} />
              </Col>
            ))}
        </>
      ) : (
        <>
          <Col
            xs={24}
            sm={24}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              padding: '30px 0',
            }}
          >
            <Empty description="Bạn không có địa chỉ giao hàng nào. Thêm địa chỉ?" />
          </Col>
          <NewAddressButton justify="center" />
        </>
      )}
    </Row>
  );
};

export default AddressList;
