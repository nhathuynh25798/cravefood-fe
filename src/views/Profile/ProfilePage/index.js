import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ProfileForm from 'components/AccountWrapper/ProfileForm';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const ProfilePage = () => {
  const ProfilePageComponent = ComponentWrapper(ProfileForm);
  return (
    <ProfilePageComponent
      data={menuItem?.menuItemAccount}
      title="Thông tin cá nhân"
    />
  );
};
export default ProfilePage;
