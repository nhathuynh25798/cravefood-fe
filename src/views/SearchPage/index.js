import React, { useEffect, useState } from 'react';
import { Row, Col, Input, notification, Empty } from 'antd';

import ProductItem from 'components/ProductItem';
import { searchProduct } from 'api/apiRouter';
import './style.scss';

const SearchPage = () => {
  const { Search } = Input;

  const [searchProducts, setSearchProducts] = useState([]);
  const [isSearched, setIsSearched] = useState(false);

  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchProducts?.[1]?.id]);

  return (
    <Row align="middle" justify="center" className="search__container">
      <Col xs={{ span: 22 }} md={{ span: 20 }} className="search__title">
        <h2>Hãy nhập từ khóa</h2>
      </Col>
      <Col xs={{ span: 22 }} md={{ span: 20 }} className="search__body">
        <Row align="middle">
          <Col xs={{ span: 24 }} md={{ span: 10 }}>
            <Search
              required
              onSearch={async (value) => {
                if (!!!value.trim().length) {
                  notification.warning({
                    message: 'Xin hãy nhập từ khoá tìm kiếm',
                  });
                  return;
                }
                const { body } = await searchProduct(value.trim());
                setSearchProducts(body);
                setIsSearched(true);
              }}
              size="large"
              enterButton
            />
          </Col>
        </Row>
      </Col>
      {searchProducts?.length > 0 ? (
        <Col xs={{ span: 22 }} md={{ span: 20 }} className="search__body">
          <Row>
            {Array.isArray(searchProducts) &&
              searchProducts?.map((it, idx) => (
                <Col xs={12} sm={3} md={6} key={idx}>
                  <ProductItem data={it} />
                </Col>
              ))}
          </Row>
        </Col>
      ) : isSearched ? (
        <Col xs={{ span: 22 }} md={{ span: 20 }} className="search__body">
          <Row justify="center">
            <Empty
              description={
                <h1 style={{ fontSize: '24px', fontWeight: 'bold' }}>
                  Không có kết quả tìm kiếm...
                </h1>
              }
            />
          </Row>
        </Col>
      ) : (
        <></>
      )}
    </Row>
  );
};
export default SearchPage;
