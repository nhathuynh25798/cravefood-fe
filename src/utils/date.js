import moment from 'moment';

export const getStartDate = () => moment().startOf('d');
