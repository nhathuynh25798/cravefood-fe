import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Modal, Button } from 'antd';
import { useDispatch } from 'react-redux';

import { actionCreators } from '../../store/cart/cart.meta';
import { calculateSalePrice } from 'utils/helper';
import currencyFormat from 'utils/currencyFormat';

import './style.scss';

const CartEditModal = (props) => {
  const { item, visible } = props;
  const dispatch = useDispatch();

  const [arrayQuantityValue, setArrayQuantityValue] = useState(item?.quantity);

  const updateQuantityFromProps = (item) => {
    setArrayQuantityValue(item?.quantity);
  };

  useEffect(() => {
    updateQuantityFromProps(item);
  }, [visible]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Modal
      title={
        <span
          style={{
            fontSize: '2rem',
            textTransform: 'uppercase',
          }}
        >
          {item?.product?.name}
        </span>
      }
      visible={visible}
      okText="Lưu vào giỏ hàng"
      cancelText="Hủy"
      onCancel={() => {
        props.onClose(false);
      }}
      onOk={() => {
        dispatch(
          actionCreators.actAddToCart({
            product: {
              id: item?.product?.id,
              name: item?.product?.name,
              images: item?.product?.images,
              discount: item?.product?.discount,
              slug: item?.product?.slug,
            },
            quantity: arrayQuantityValue,
          })
        );
        props.onClose(false);
      }}
      okButtonProps={{
        disabled:
          arrayQuantityValue.reduce(
            (prev, curr) => prev + curr?.quantity,
            0
          ) === 0
            ? true
            : false,
      }}
    >
      <Row className="cart-form__container" gutter={[0, 16]}>
        {arrayQuantityValue
          ?.sort((a, b) => a?.dishSize?.sizeId - b?.dishSize?.sizeId)
          ?.map((it, idx) => (
            <Col xs={{ span: 24 }} key={idx}>
              <Row gutter={[0, 12]}>
                <Col xs={{ span: 12 }} className="cart-form__price">
                  <span
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      fontSize: '1.5rem',
                      lineHeight: '2.25rem',
                    }}
                  >
                    <img
                      src={
                        process.env.REACT_APP_BACKEND_URL +
                        it?.dishSize?.sizeImage
                      }
                      alt="m-size"
                    />
                    &nbsp; {it?.dishSize?.sizeName}
                  </span>
                </Col>
                <Col xs={{ span: 12 }} className="cart-form__price">
                  <Row
                    justify="space-between"
                    align="center"
                    style={{ width: '100%' }}
                  >
                    <Col
                      xs={{ span: 8 }}
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Button
                        className="cart-form-button cart-form-button-down"
                        onClick={() => {
                          arrayQuantityValue[idx].quantity -= 1;
                          setArrayQuantityValue([...arrayQuantityValue]);
                          if (arrayQuantityValue[idx].quantity <= 0) {
                            arrayQuantityValue[idx].quantity = 0;
                            setArrayQuantityValue([...arrayQuantityValue]);
                          }
                        }}
                      >
                        <img src="/assets/images/minus.svg" alt="minus" />
                      </Button>
                    </Col>
                    <Col
                      xs={{ span: 8 }}
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Input
                        size="middle"
                        pattern="[0-9]*"
                        value={arrayQuantityValue[idx].quantity}
                        className="cart-form-quantity"
                      />
                    </Col>
                    <Col
                      xs={{ span: 8 }}
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Button
                        className="cart-form-button cart-form-button-up"
                        onClick={() => {
                          arrayQuantityValue[idx].quantity += 1;
                          setArrayQuantityValue([...arrayQuantityValue]);
                          if (arrayQuantityValue[idx].quantity >= 50) {
                            arrayQuantityValue[idx].quantity = 50;
                            setArrayQuantityValue([...arrayQuantityValue]);
                          }
                        }}
                      >
                        <img src="/assets/images/plus.svg" alt="s-size" />
                      </Button>
                    </Col>
                  </Row>
                </Col>
                <Col xs={{ span: 24 }} className="cart-form__price">
                  <Row style={{ width: '100%' }}>
                    <Col
                      xs={{ span: 12 }}
                      style={{
                        color: 'rgb(120, 120, 120)',
                        textDecoration: 'line-through',
                      }}
                    >
                      {currencyFormat(
                        it?.dishSize?.price * it?.quantity ||
                          it?.dishSize?.price
                      )}
                      &nbsp;
                    </Col>
                    <Col xs={{ span: 12 }}>
                      {currencyFormat(
                        calculateSalePrice(
                          item?.product?.discount,
                          it?.dishSize?.price * it?.quantity ||
                            it?.dishSize?.price
                        )
                      )}
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          ))}
      </Row>
    </Modal>
  );
};

export default CartEditModal;
