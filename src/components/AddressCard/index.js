import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Modal, Card, notification } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { navigate } from '@reach/router';

import { actionCreator } from 'store/user/user.meta';
import { actionCreators } from 'store/cart/cart.meta';
import AddressForm from 'components/AddressForm';
import './style.scss';
import 'antd/dist/antd.css';

const AddressCard = (props) => {
  const { address = {}, selected, showSelected = true } = props;
  const active = selected === address?.id;
  const [visible, setVisible] = useState(false);
  const dispatch = useDispatch();
  const { confirm } = Modal;

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/');
    }
  }, [selected]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleShowModal = () => {
    setVisible(!visible);
  };

  const dataAddressForm = [
    {
      title: 'Họ tên:',
      content: address?.name,
    },
    {
      title: 'Số điện thoaị:',
      content: address?.phone,
    },
    {
      title: 'Địa chỉ',
      content: `${
        (address?.detail, address?.ward, address?.district, address?.province)
      }`,
    },
    {
      title: 'Ghi chú:',
      content: address?.note,
    },
  ];

  return (
    <Card className="address-card">
      <Row>
        <Col
          xs={24}
          sm={24}
          style={{
            padding: '3.2rem',
            border: active ? '1px solid var(--primary)' : 'none',
            wordBreak: 'break-all',
            backgroundColor: 'var(--quaternary)',
            cursor: 'pointer',
          }}
        >
          <Row>
            <Col style={{ flex: '1 1 0%' }}>
              <Row gutter={[0, 24]}>
                {dataAddressForm?.map(
                  (it, idx) =>
                    it?.content && (
                      <Col xs={24} sm={24} key={idx}>
                        <Row>
                          <span className="address-card__text">
                            <strong>{it?.title}</strong>
                          </span>
                        </Row>
                        <Row>
                          <span className="address-card__text">
                            {it?.content}
                          </span>
                        </Row>
                      </Col>
                    )
                )}
              </Row>
            </Col>
            {!!address?.defaultAddressValue && (
              <Col>
                <span style={{ fontSize: '1.6rem', color: 'var(--primary)' }}>
                  Mặc định
                </span>
              </Col>
            )}
          </Row>
          <Row>
            <Col xs={24} sm={24}>
              <Row>
                <Col xs={12} sm={12}>
                  <Button
                    onClick={handleShowModal}
                    size="medium"
                    className="address-card__button address-card__button--edit"
                  >
                    Thay đổi
                  </Button>
                  <Modal
                    className="address-modal"
                    visible={visible}
                    onCancel={() => setVisible(false)}
                  >
                    <AddressForm
                      onFinish={(values) => setVisible(values)}
                      address={address}
                    />
                  </Modal>
                  <Button
                    size="medium"
                    className="address-card__button address-card__button--remove"
                    onClick={() => {
                      confirm({
                        okText: 'Đồng ý',
                        cancelText: 'Hủy',
                        title: 'Bạn có muốn xóa địa chỉ này?',
                        onOk() {
                          if (address?.defaultAddressValue) {
                            notification.warning({
                              message: 'Địa chỉ mặc định không thể xoá!',
                            });
                            return;
                          }
                          dispatch(
                            actionCreator.actRemoveAddress({
                              userId: address?.userId,
                              id: address?.id,
                            })
                          );
                        },
                      });
                    }}
                  >
                    Xóa
                  </Button>
                </Col>

                {showSelected && (
                  <Col xs={12} sm={12}>
                    <Row justify="end">
                      <span
                        style={{
                          fontSize: '1.6rem',
                          color: active
                            ? 'var(--primary)'
                            : 'rgb(196, 196, 196)',
                        }}
                        onClick={() => {
                          dispatch(actionCreators.actSelectAddress(address));
                          props.onSelected(address?.id);
                        }}
                      >
                        Giao tới địa chỉ này
                      </span>
                    </Row>
                  </Col>
                )}
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Card>
  );
};
export default AddressCard;
