import React from 'react';
import { Menu } from 'antd';
import { navigate } from '@reach/router';

import slugify from 'utils/slugify';

import 'antd/dist/antd.css';
import './style.scss';

const NavMenu = (props) => {
  const { data, title } = props;

  return (
    <Menu
      style={{ width: '100%' }}
      defaultSelectedKeys={[slugify(title)]}
      defaultOpenKeys={[slugify(title)]}
      theme="light"
      mode="inline"
    >
      {data.map((item) => (
        <Menu.Item
          key={slugify(item?.title)}
          onClick={() => {
            navigate(item?.linkTo);
          }}
        >
          {item?.title}
        </Menu.Item>
      ))}
    </Menu>
  );
};
export default NavMenu;
