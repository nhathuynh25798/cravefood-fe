import { Spin } from 'antd';
import styled from 'styled-components';
import { width, height } from 'styled-system';

const StyledLoadingSVG = styled(Spin)`
  position: fixed;
  top: 50%;
  left: 50%;
  ${width};
  ${height}
  z-index: 9999;
`;

export default StyledLoadingSVG;
