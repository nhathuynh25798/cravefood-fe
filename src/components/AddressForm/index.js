import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Form, Input, Select, Checkbox, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { navigate } from '@reach/router';

import { actionCreators as locationAction } from 'store/location/location.meta';
import { actionCreator } from 'store/user/user.meta';
import HeadPhoneNumber from 'components/AccountWrapper/HeadPhoneNumber';

import './style.scss';

const AddressForm = (props) => {
  const [form] = Form.useForm();
  const { address = {} } = props;
  const { Option } = Select;
  const dispatch = useDispatch();

  const { provinces = [] } = useSelector((store) => store.location);
  const { addresses = [], user = {} } = useSelector((store) => store.user);

  const [provinceData, setProvincesData] = useState(provinces);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  const fetchProvincesData = () => {
    dispatch(locationAction.actFetchProvinces());
    setDistricts(provinces?.[0]?.districts);
  };

  useEffect(() => {
    if (!user?.id) {
      navigate('/');
    } else {
      fetchProvincesData();
    }
  }, [address?.id]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setProvincesData(provinces);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleProvinceChange = (value, key) => {
    setDistricts(
      provinces?.filter((it) => {
        return it?.id === +key?.key;
      })?.[0]?.districts
    );
  };

  const handleDistrictChange = (value, key) => {
    setWards(
      districts?.filter((it) => {
        return it?.id === +key?.key;
      })?.[0]?.wards
    );
  };

  const setDefaultValues = (values) => {
    const {
      name,
      detail,
      ward,
      district,
      province,
      phone,
      note,
      defaultAddress,
    } = values;
    if (address?.id) {
      const index = addresses.findIndex((x) => x.id === address.id);
      if (index !== -1) {
        addresses[index] = {
          ...addresses[index],
          name,
          detail,
          ward,
          district,
          province,
          phone,
          note,
          defaultAddressValue: 0,
        };
        if (defaultAddress) {
          addresses.forEach((item) => (item.defaultAddressValue = 0));
          addresses[index] = {
            ...addresses[index],
            defaultAddressValue: 1,
            name,
            detail,
            ward,
            district,
            province,
            phone,
            note,
          };
        }
      }
    } else {
      if (defaultAddress) {
        addresses.forEach((item) => (item.defaultAddressValue = 0));
        addresses.unshift({
          userId: user?.id,
          name,
          detail,
          ward,
          district,
          province,
          phone,
          note,
          enable: 1,
          defaultAddressValue: 1,
        });
      } else {
        addresses.push({
          userId: user?.id,
          name,
          detail,
          ward,
          district,
          province,
          phone,
          note,
          enable: 1,
          defaultAddressValue: 0,
        });
      }
    }
    return [...addresses];
  };

  const onFinish = (values) => {
    const array = setDefaultValues(values);
    dispatch(actionCreator.actAddAddress({ userId: user?.id, data: array }));
    props.onFinish(false);
  };

  return (
    <Row justify="center">
      <Col xs={{ span: 24 }}>
        <Card bordered>
          <Form form={form} scrollToFirstError onFinish={onFinish}>
            <Row className="address-form__container">
              <Col span={24} className="address-form__name">
                <h1 style={{ fontSize: 30 }}>Địa chỉ giao hàng</h1>
              </Col>
              <Col xs={{ span: 24 }}>
                <Row justify="space-between">
                  <Col xs={{ span: 12 }} style={{ padding: '0 0.5rem' }}>
                    <Row>
                      <Col
                        span={24}
                        className="address-form__item"
                        style={{ marginTop: '2.4rem' }}
                      >
                        <span>Họ và tên</span>
                        <Form.Item
                          name="name"
                          rules={[
                            {
                              required: true,
                              message: 'Vui lòng điền họ và tên người nhận',
                            },
                          ]}
                          initialValue={address?.name ?? ''}
                        >
                          <Input size="large" />
                        </Form.Item>
                      </Col>

                      <Col span={24} className="address-form__item">
                        <span style={{ marginBottom: '0.8rem' }}>
                          Số điện thoại
                        </span>
                        <Form.Item
                          name="phone"
                          rules={[
                            {
                              pattern: '^0[0-9]{9}$',
                              required: true,
                              message: 'Vui lòng nhập số điện thoại',
                            },
                          ]}
                          className="account-input"
                          initialValue={address?.phone ?? ''}
                        >
                          <Input
                            size="large"
                            addonBefore={<HeadPhoneNumber />}
                            style={{ width: '100%' }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>

                  <Col xs={{ span: 12 }} style={{ padding: '0 0.5rem' }}>
                    <Row>
                      <Col
                        span={24}
                        className="address-form__item"
                        style={{ marginTop: '2.4rem' }}
                      >
                        <span>Tỉnh/Thành Phố</span>
                        <Form.Item
                          name="province"
                          initialValue={
                            address?.province ?? provinces?.[0]?.name
                          }
                        >
                          <Select size="large" onChange={handleProvinceChange}>
                            {provinceData
                              ?.sort((a, b) => a?.id - b?.id)
                              ?.map((province) => (
                                <Option
                                  key={province?.id}
                                  value={province?.name}
                                >
                                  {province?.name}
                                </Option>
                              ))}
                          </Select>
                        </Form.Item>
                      </Col>

                      <Col xs={{ span: 24 }}>
                        <Row>
                          <Col xs={{ span: 12 }}>
                            <Row>
                              <Col span={22} className="address-form__item">
                                <span>Quận/Huyện</span>
                                <Form.Item
                                  name="district"
                                  rules={[
                                    {
                                      required: true,
                                      message: 'Vui lòng chọn Quận/Huyện',
                                    },
                                  ]}
                                  className="account-input"
                                  initialValue={address?.district ?? ''}
                                >
                                  <Select
                                    size="large"
                                    onChange={handleDistrictChange}
                                  >
                                    {districts
                                      ?.sort((a, b) => a?.id - b?.id)
                                      ?.map((district) => (
                                        <Option
                                          key={district?.id}
                                          value={district?.name}
                                        >
                                          {district?.name}
                                        </Option>
                                      ))}
                                  </Select>
                                </Form.Item>
                              </Col>
                            </Row>
                          </Col>

                          <Col xs={{ span: 12 }}>
                            <Row justify="end">
                              <Col span={22} className="address-form__item">
                                <span>Phường/Xã</span>
                                <Form.Item
                                  name="ward"
                                  rules={[
                                    {
                                      required: true,
                                      message: 'Vui lòng chọn Phường/Xã',
                                    },
                                  ]}
                                  className="account-input"
                                  initialValue={address?.ward ?? ''}
                                >
                                  <Select size="large">
                                    {wards
                                      ?.sort((a, b) => a?.id - b?.id)
                                      ?.map((ward) => (
                                        <Option
                                          key={ward?.id}
                                          value={ward?.name}
                                        >
                                          {ward?.name}
                                        </Option>
                                      ))}
                                  </Select>
                                </Form.Item>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>

                      <Col span={24} className="address-form__item">
                        <span>Địa chỉ cụ thể</span>
                        <Form.Item
                          name="detail"
                          rules={[
                            {
                              required: true,
                              message: 'Vui lòng điền Địa chỉ cụ thể',
                            },
                          ]}
                          initialValue={address?.detail ?? ''}
                        >
                          <Input size="large" />
                        </Form.Item>
                      </Col>

                      <Col span={24} className="address-form__item">
                        <Form.Item
                          name="defaultAddress"
                          valuePropName="checked"
                          initialValue={address?.defaultAddressValue ?? false}
                        >
                          <Checkbox>Đặt làm địa chỉ mặc định</Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>

                    <Col
                      span={24}
                      className="address-form__item"
                      style={{ marginTop: '2.4rem' }}
                    >
                      <span>Ghi chú thêm khi giao hàng (nếu có)</span>
                      <Form.Item name="note" initialValue={address?.note ?? ''}>
                        <Input size="large" />
                      </Form.Item>
                    </Col>
                  </Col>
                  <Col xs={{ span: 24 }}>
                    <Button
                      htmlType="submit"
                      type="primary"
                      size="large"
                      style={{ borderRadius: '0.4rem' }}
                    >
                      Lưu địa chỉ
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default AddressForm;
