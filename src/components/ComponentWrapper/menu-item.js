export default {
  menuItemSupport: [
    {
      title: 'Thông tin chuyển khoản',
      linkTo: '/ho-tro-khach-hang/thong-tin-chuyen-khoan',
    },
    {
      title: 'Chính sách đổi trả',
      linkTo: '/ho-tro-khach-hang/chinh-sach-doi-tra',
    },
    {
      title: 'Điều khoản sử dụng',
      linkTo: '/ho-tro-khach-hang/dieu-khoan-su-dung',
    },
    {
      title: 'Chính sách bảo mật',
      linkTo: '/ho-tro-khach-hang/chinh-sach-bao-mat',
    },
    {
      title: 'Thông tin giao hàng',
      linkTo: '/ho-tro-khach-hang/thong-tin-giao-hang',
    },
    {
      title: 'Hướng dẫn đặt hàng',
      linkTo: '/ho-tro-khach-hang/huong-dan-dat-hang',
    },
  ],
  menuItemAccount: [
    {
      title: 'Thông tin cá nhân',
      linkTo: '/thong-tin-ca-nhan',
    },
    {
      title: 'Quản lý đơn hàng',
      linkTo: '/danh-sach-don-hang',
    },
    {
      title: 'Địa chỉ giao hàng',
      linkTo: '/dia-chi-giao-hang',
    },
    {
      title: 'Danh sách yêu thích',
      linkTo: '/danh-sach-yeu-thich',
    },
  ],
};
