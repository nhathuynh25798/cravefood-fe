import React from 'react';
import { Row, Col, Divider } from 'antd';

import NavMenu from '../../components/NavMenu';
import 'antd/dist/antd.css';
import './style.scss';

const ComponentWrapper = (ComponentWrapped) => (props) => {
  return (
    <Row justify="center" style={{ margin: '0.8rem' }}>
      <Col xs={{ span: 22, offset: 1 }} style={{ padding: '0.8rem' }}>
        <Row justify="space-between">
          <Col md={{ span: 6 }} style={{ padding: '0.8rem' }}>
            <NavMenu data={props?.data} title={props?.title} />
          </Col>
          <Col
            xs={{ span: 24 }}
            md={{ span: 18 }}
            style={{ padding: '0.8rem' }}
          >
            <Row
              style={{
                backgroundColor: 'var(--quaternary)',
                borderRadius: '0.4rem',
              }}
            >
              <Col
                xs={{ span: 24 }}
                style={{
                  padding: '2.2rem',
                }}
              >
                <span style={{ fontSize: '2.2rem', fontWeight: '550' }}>
                  {props?.title}
                </span>
              </Col>
              <Divider></Divider>
              <Col
                xs={{ span: 24 }}
                style={{
                  padding: '2.2rem',
                  paddingTop: '0',
                }}
              >
                <ComponentWrapped {...props} />
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default ComponentWrapper;
