import React from 'react';
import { Row, Col } from 'antd';

import './style.scss';

const AccountWrapper = (WrappedComponent) => (props) => {
  return (
    <Row
      justify="center"
      align="center"
      className="account__container"
      style={{
        backgroundImage:
          props?.backgroundImage ??
          'url("/assets/images/user-login-background.png")',
      }}
    >
      <Col
        xs={{ span: 24 }}
        md={{ span: 14 }}
        lg={{ span: 12 }}
        xl={{ span: 10 }}
        className="account__form"
      >
        <WrappedComponent {...props} />
      </Col>
    </Row>
  );
};
export default AccountWrapper;
