const sizeMapping = {
  small: 'Size nhỏ',
  medium: 'Size vừa',
  large: 'Size lớn',
  one: 'Giá',
};

const sizeServiceMapping = {
  small: '/static/images/ssize.svg',
  medium: '/static/images/msize.svg',
  large: '/static/images/lsize.svg',
  one: '/static/images/price.svg',
};

export default function(size) {
  return sizeMapping?.[size] || 'Size';
}

export const sizeService = size => {
  return sizeServiceMapping?.[size] || 'Size';
};
